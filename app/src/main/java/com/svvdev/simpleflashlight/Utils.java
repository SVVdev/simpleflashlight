package com.svvdev.simpleflashlight;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.List;

public class Utils {

    // Declaring your view and variables
	private static Boolean mPrefDevice;
    private static Camera mCamera = null;
    private static SharedPreferences mPreferences;

	/**
     * Check if the device has a camera flash.
     *
     * @return The preference value.
     */
    public static boolean deviceHasCameraFlash(Context context) {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            mCamera = Camera.open();
            if (mCamera == null) {
                mPrefDevice = mPreferences.edit().putBoolean("mPrefDevice", false).commit();
                return mPrefDevice;
            }
            Camera.Parameters mParameters = mCamera.getParameters();
            if (mParameters.getFlashMode() == null) {
                mPrefDevice = mPreferences.edit().putBoolean("mPrefDevice", false).commit();
                return mPrefDevice;
            }
            List<String> mSupportedFlashModes = mParameters.getSupportedFlashModes();
            if (!mSupportedFlashModes.contains(Camera.Parameters.FLASH_MODE_TORCH)) {
                mPrefDevice = mPreferences.edit().putBoolean("mPrefDevice", false).commit();
                return mPrefDevice;
            }
            mCamera.release();
            mCamera = null;
        } catch (Exception e) {
            Log.e("Error : ", e+"");
        }
        mPrefDevice = mPreferences.edit().putBoolean("mPrefDevice", true).commit();
        return mPrefDevice;
    }

	/**
     * Gets the preference color.
     *
     * @return The preference color.
     */
    public static int getPrefColor(Context context, String preference) {

        int mColor = context.getResources().getColor(R.color.red);
		
        if (preference.equals(context.getString(R.string.red))) {
            mColor = context.getResources().getColor(R.color.red);
            return mColor;
        } else if (preference.equals(context.getString(R.string.pink))) {
            mColor = context.getResources().getColor(R.color.pink);
            return mColor;
        } else if (preference.equals(context.getString(R.string.purple))) {
            mColor = context.getResources().getColor(R.color.purple);
            return mColor;
        } else if (preference.equals(context.getString(R.string.deepPurple))) {
            mColor = context.getResources().getColor(R.color.deepPurple);
            return mColor;
        } else if (preference.equals(context.getString(R.string.indigo))) {
            mColor = context.getResources().getColor(R.color.indigo);
            return mColor;
        } else if (preference.equals(context.getString(R.string.blue))) {
            mColor = context.getResources().getColor(R.color.blue);
            return mColor;
        } else if (preference.equals(context.getString(R.string.lightBlue))) {
            mColor = context.getResources().getColor(R.color.lightBlue);
            return mColor;
        } else if (preference.equals(context.getString(R.string.cyan))) {
            mColor = context.getResources().getColor(R.color.cyan);
            return mColor;
        } else if (preference.equals(context.getString(R.string.teal))) {
            mColor = context.getResources().getColor(R.color.teal);
            return mColor;
        } else if (preference.equals(context.getString(R.string.green))) {
            mColor = context.getResources().getColor(R.color.green);
            return mColor;
        } else if (preference.equals(context.getString(R.string.lightGreen))) {
            mColor = context.getResources().getColor(R.color.lightGreen);
            return mColor;
        } else if (preference.equals(context.getString(R.string.lime))) {
            mColor = context.getResources().getColor(R.color.lime);
            return mColor;
        } else if (preference.equals(context.getString(R.string.yellow))) {
            mColor = context.getResources().getColor(R.color.yellow);
            return mColor;
        } else if (preference.equals(context.getString(R.string.amber))) {
            mColor = context.getResources().getColor(R.color.amber);
            return mColor;
        } else if (preference.equals(context.getString(R.string.orange))) {
            mColor = context.getResources().getColor(R.color.orange);
            return mColor;
        } else if (preference.equals(context.getString(R.string.deepOrange))) {
            mColor = context.getResources().getColor(R.color.deepOrange);
            return mColor;
        } else if (preference.equals(context.getString(R.string.brown))) {
            mColor = context.getResources().getColor(R.color.brown);
            return mColor;
        } else if (preference.equals(context.getString(R.string.grey))) {
            mColor = context.getResources().getColor(R.color.grey);
            return mColor;
        } else if (preference.equals(context.getString(R.string.blueGrey))) {
            mColor = context.getResources().getColor(R.color.blueGrey);
            return mColor;
        }
        return mColor;
    }
	
	public static void setMainTheme(Activity activity) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.setTheme(R.style.MaterialTranslucent);
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            activity.setTheme(R.style.HoloTranslucent);
        } else {
            activity.setTheme(R.style.HoloWithoutBar);
        }
	}

}
