package com.svvdev.simpleflashlight;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.List;

public class TorchSwitch extends BroadcastReceiver {

    // Declaring your view and variables
	private static final String TAG = "TorchSwitch";
    public static final String TOGGLE_FLASHLIGHT = "com.svvdev.simpleflashlight.TOGGLE_FLASHLIGHT";
    public static final String TORCH_STATE_CHANGED = "com.svvdev.simpleflashlight.TORCH_STATE_CHANGED";

    @Override
    public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "onReceive");

        if (intent.getAction().equals(TOGGLE_FLASHLIGHT)) {

            Intent mIntent = new Intent(context, TorchService.class);
            if (this.isTorchServiceRunning(context))
                context.stopService(mIntent);
            else
                context.startService(mIntent);

        }
    }

    private boolean isTorchServiceRunning(Context context) {

        ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        List<RunningServiceInfo> mList = mActivityManager.getRunningServices(100);

        if (!(mList.size() > 0)) {
            return false;
		}
        for (RunningServiceInfo mServiceInfo : mList) {
            ComponentName mServiceName = mServiceInfo.service;
            if (mServiceName.getClassName().endsWith(".TorchService")
                    || mServiceName.getClassName().endsWith(".RootTorchService"))
                return true;
        }
        return false;
    }
}
