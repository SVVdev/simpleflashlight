package com.svvdev.simpleflashlight;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends Activity {

    // Declaring your view and variables
    private AdView mAdView;
    private static final String TAG = "MainActivity";
	private static MainActivity mActivity;
    private boolean mTorchOn;
    private boolean mValue;
    private float mFullScreenScale;
	private ImageView mImageViewShape;
    private ImageView mImageViewOff;
    private ImageView mImageViewOn;
    private Context mContext;
    private SharedPreferences mPreferences;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = this;
        mContext = this.getApplicationContext();
        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        setThemeColor();

        setContentView(R.layout.activity_main);

        // checking if camera is available
        openFirstTime();

        mTorchOn = false;

        mImageViewShape = (ImageView) findViewById(R.id.imageViewShape);
        mImageViewOff = (ImageView) findViewById(R.id.imageViewOff);
        mImageViewOn = (ImageView) findViewById(R.id.imageViewOn);

        mImageViewOff.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                onCreateIntent();
            }
        });

        // ad BANNER
        mAdView = (AdView)findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
    }

    private void onCreateIntent() {
        Log.d(TAG, "onCreateIntent");
        Intent mIntent = new Intent(TorchSwitch.TOGGLE_FLASHLIGHT);
        mIntent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        getApplicationContext().sendBroadcast(mIntent);
    }

	@Override
    public void onPause() {
		super.onPause();
        getApplicationContext().unregisterReceiver(mStateReceiver);
    }

	@Override
    public void onResume() {
		Log.d(TAG, "onResume");
		super.onResume();
        getApplicationContext().registerReceiver(mStateReceiver, new IntentFilter(TorchSwitch.TORCH_STATE_CHANGED));
        setThemeColor();
        setShapeColor();
    }

    private void setThemeColor() {
        Utils.setMainTheme(mActivity);
    }

    private void setShapeColor() {
        String mPrefColor = mPreferences.getString("color", getString(R.string.red));
        GradientDrawable mShapeDrawable = (GradientDrawable) mImageViewShape.getDrawable();
        mShapeDrawable.setColor(Utils.getPrefColor(this, mPrefColor));
    }

    private void setFlashOn() {



        String mPrefColor = mPreferences.getString("color", getString(R.string.red));
        Boolean mPrefScreen = mPreferences.getBoolean("screen", false);
        Boolean mPrefDevice = mPreferences.getBoolean("mPrefDevice", false);
        Boolean mPrefBright = mPreferences.getBoolean("mPrefBright", false);

        Window mWindow = getWindow();
        WindowManager.LayoutParams mSettings = mWindow.getAttributes();
        GradientDrawable mShape = (GradientDrawable) mImageViewShape.getDrawable();

        if (mImageViewShape == null) {
            return;
        }
        if (!mPrefDevice||mPrefScreen) {
            //hideSystemUi(mImageViewOff);
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mSettings.screenBrightness = 1f; // Set 100% brightness
            if (mPrefBright) {
                mShape.setColorFilter(Utils.getPrefColor(mContext, mPrefColor), PorterDuff.Mode.SRC_ATOP);
            } else {
                mShape.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            }
            getWindow().setAttributes(mSettings);
        }
        if (mFullScreenScale <= 0.0f) {
            mFullScreenScale = getMeasureScale();
        }

        // SVV do image change
        mImageViewOff.setImageResource(R.drawable.img_on);

    }

    private void setFlashOff() {



        Boolean mPrefScreen = mPreferences.getBoolean("screen", false);
        Boolean mPrefDevice = mPreferences.getBoolean("mPrefDevice", false);

        Window mWindow = getWindow();
        WindowManager.LayoutParams mSettings = mWindow.getAttributes();
        GradientDrawable mShape = (GradientDrawable) mImageViewShape.getDrawable();

        if (mImageViewShape == null) {
            return;
        }
        if (!mPrefDevice||mPrefScreen) {
            //showSystemUi(mImageViewOff);
            mWindow.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mSettings.screenBrightness = -1f; // Set automatic brightness
            mShape.clearColorFilter();
            getWindow().setAttributes(mSettings);
        }

        // SVV do image change
        mImageViewOff.setImageResource(R.drawable.img_off);

    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        mFullScreenScale = getMeasureScale();
    }

    private BroadcastReceiver mStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(TorchSwitch.TORCH_STATE_CHANGED)) {
                mTorchOn = intent.getIntExtra("state", 0) != 0;
                if (mTorchOn) {
                    setFlashOn();
                } else {
                    setFlashOff();
                }
            }
        }
    };

    private float getMeasureScale() {
        WindowManager mWindowManager = getWindowManager();
        Display mDisplay = mWindowManager.getDefaultDisplay();
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        mDisplay.getMetrics(mDisplayMetrics);

        float mDisplayHeight = mDisplayMetrics.heightPixels;
        float mDisplayWidth = mDisplayMetrics.widthPixels;
        return (Math.max(mDisplayHeight, mDisplayWidth) /
                getApplicationContext().getResources().getDimensionPixelSize(R.dimen.size_shape)) * 2;
    }

    // Create AlertDialog for the first launch
    private void openFirstTime() {
        mValue = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("mValue", true);
        if (mValue) {
            Utils.deviceHasCameraFlash(mContext);
            getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit().putBoolean("mValue", false).commit();
        }
    }




}
