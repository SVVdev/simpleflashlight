package com.svvdev.simpleflashlight;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class TorchService extends Service {

    // Declaring your view and variables
    private static final String TAG = "TorchService";
    private TimerTask mTorchTask;
    private Timer mTorchTimer;
    private Context mContext;
    private SharedPreferences mPreferences;

	@Override
    public void onCreate() {
        Log.d(TAG, "onCreate");

        mContext = getApplicationContext();

        mTorchTask = new TimerTask() {
            public void run() {

                mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
                Boolean mPrefScreen = mPreferences.getBoolean("screen", false);

                if (mPrefScreen) {
                    FlashDevice.getInstance(mContext).setFlashMode(FlashDevice.OFF);
                } else {
                    FlashDevice.getInstance(mContext).setFlashMode(FlashDevice.ON);
                }
            }
        };

        mTorchTimer = new Timer();

    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");

        if (intent == null) {
            stopSelf();
            return START_NOT_STICKY;
        }


        Log.d(TAG, "onStartCommand");

            mTorchTimer.schedule(mTorchTask, 0, 100);


        updateState(true);
        return START_STICKY;
    }

	@Override
    public void onDestroy() {
        mTorchTimer.cancel();
        mTorchTimer = null;
        FlashDevice.getInstance(mContext).setFlashMode(FlashDevice.OFF);
        updateState(false);
    }

    private void updateState(boolean on) {
        Intent mIntent = new Intent(TorchSwitch.TORCH_STATE_CHANGED);
        mIntent.putExtra("state", on ? 1 : 0);
        sendStickyBroadcast(mIntent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

